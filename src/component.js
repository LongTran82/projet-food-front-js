module.exports = (state) => function Component(mountPoint, render, initEvents) {
  this.mountPoint = mountPoint
  this.render = render
  this.state = state
  if (initEvents && typeof initEvents === "function")
    initEvents(this)
}
