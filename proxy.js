// state
let state = new Proxy(
    {observers:[], addObserver: function(o) {this.observers.push(o)}},
    {
    set: function (target, prop, val) {
        target[prop] = val
        target.observers.forEach(o => o.render(target))
    },
    get: function(target, prop){
        console.log(`proxy is calling : ${prop}`)
        return target[prop]
    }
})

// fetch data
const backend = 'http://localhost:2999'
let data = fetch(`${backend}/food`)
    .then(res => res.json())
    .then(list => list.map(p => new Food(backFood2food(p))))
    .then(plats => { window.plats = plats; console.log(plats); return plats })
    .then(plats => state.plats = plats)

function COmponent(menuPoints, render) {
    this.mountPoint = mountPoint
      }
      

// list component
let listComp = {
    mountPoint: listMountPoint,
    render: function (state) {
        this.mountPoint.innerHTML = plats2ul(state.plats).innerHTML
    }
}

// counter component
let counterComp = {
    mountPoint: counterMountPoint,
    render: function (state) {
        this.mountPoint.innerHTML = `${state.plats.length} aliments`
    }
}

state.addObserver(listComp)
state.addObserver(counterComp)

