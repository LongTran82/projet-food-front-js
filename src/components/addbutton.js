module.exports = (emit, mountPoint) => {
  const Component = require('../component')(state)
  return new Component(
    mountPoint,
    function(state){
      this.mountPoint.type = 'button'
      this.mountPoint.innerText = 'Créer un aliment'
      this.mountPoint.hidden = !state.view.includes(this)

    },
    comp =>
      comp.mountPoint.addEventListener('click', () => {
        // comp.state.view = comp.state.views.addFood
        emit('addfood button clicked')
      })
  )
}
