* Initialiser des données dans la base

- [[file:init.html][init.html]] : référence ~init_with_mods~.js
- [[file:init.js][init.js]] : utilise le module [[file:src/models/food.js][src/models/food.js]] avec ~require~

Pour transformer ~init.js => init_with_mods.js~:

#+begin_src shell
browserify init.js -o init_with_mods.js
#+end_src


À chaque modification d'~init.js~, il faut relancer la commande ~browserify~.

