const template = `
  <label>Nom
    <input type="text" name="name" size="20"/>
  </label>
  <label>Description
    <input type="text" name="description" size="20" />
  </label>
  <label>Weight
  <input type="text" name="weight" size="5" />
  </label>
  <label>Glucides
  <input type="text" name="carbohydrates" size="5" />
  </label>
  <label>Protéines
  <input type="text" name="proteins" size="5" />
  </label>
  <label>Lipides
    <input type="text" name="lipids" size="5" />
  </label>
  <input type="submit" name="submit" value="Créer" />
`

module.exports = (emit, mountPoint) => {
  const Component = require('../component')(state)
  return new Component(
    mountPoint,
    function(state){
      this.mountPoint.hidden = !state.view.includes(this)
      this.mountPoint.innerHTML = template
      this.mountPoint.querySelector('input').focus()
    },
    comp =>
      comp.mountPoint.addEventListener('submit', (e) => {
        e.preventDefault()
        let newResource = Object.fromEntries(new FormData(comp.mountPoint))
        console.log(newResource)
        emit('create new ressource', newResource)
      })
  )
}
