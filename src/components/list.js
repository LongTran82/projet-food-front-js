const plats2ul = (plats) => {
  const ul = document.createElement('ul')
  ul.id = "foodlist"
  ul.innerHTML = plats.map(plat2li).join('')
  return ul
}

const plat2li = (plat) => `
  <li class="plat" data-weight="${plat.weight}" data-calories="${plat.caloriesTotal}">
    <p><span>${plat.name} - </span>
    <a data-action="show" href="/food/${plat.id}" id="show_plat">Show</a>
    <a data-action="edit" href="/food/${plat.id}" id="edit_plat">Edit</a>
    <a data-action="delete" href="/food/${plat.id}" id="delete_plat">Delete</a>

    </p>
  </li>
`
module.exports = (emit, mountPoint) => {
  const Component = require('../component')(state)
  return new Component(
    mountPoint,
    function (state){
      this.mountPoint.innerHTML = plats2ul(state.plats).innerHTML
    },
    comp => {
      comp.mountPoint.addEventListener('click', (e) => {
        e.preventDefault()
        if (e.target.matches('#show_plat')){
          const targetUrl = e.target.attributes.href.value
          emit('show', targetUrl)
        }

        if (e.target.matches('#edit_plat')){
          const targetUrl = e.target.attributes.href.value
          emit('edit element', targetUrl)
        }

          if (e.target.matches('#delete_plat')){
          const targetUrl = e.target.attributes.href.value
          emit('delete resource', targetUrl)
        }
      })
    }
  )
}
