module.exports = (state, mountPoint) => {
  const Component = require('../component')(state)
  return new Component(
    mountPoint,
    function(state){
      this.mountPoint.innerHTML = `${state.plats.length} aliments`
      this.mountPoint.hidden = !state.view.includes(this)
    }
  )
}
