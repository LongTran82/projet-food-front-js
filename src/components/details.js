const platDetails = (plat) =>
`
<p>Nom du plat : ${plat.name}</p>
<p>Poids : ${plat.weight}</p>
<p>Calories : ${plat.calories}</p>
<p>Protéines : ${plat.proteines}</p>
<p>Glucides : ${plat.glucides}</p>
<p>Lipides : ${plat.lipides}</p>
`

module.exports = (state, mountPoint) => {
    const Component = require('../component')(state)
    return new Component(
      mountPoint,
      function (state){
        this.mountPoint.hidden = !state.view.includes(this)
        this.mountPoint.innerHTML = platDetails(state.element)
      }
    )
  }
  