class Food{
  constructor(nameOrObject, weight, caloriesTotal, lipides, glucides, proteines){
    if (nameOrObject instanceof Object) {
      Object.assign(this, nameOrObject)
    } else {
      this.name = nameOrObject
      this.weight = weight
      this.lipides = lipides
      this.glucides = glucides
      this.proteines = proteines
    }
  }
  get calories () { return this.lipides*9+this.glucides*4+this.proteines*4}
}

Object.defineProperty(Food.prototype,'caloriesTotal', {
  get: function(){
    return  this.calories * this.weight / 100
  }
})



Food.message = 'this is in food module'

module.exports = Food 

